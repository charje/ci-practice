(in-package #:hello)

(define-easy-handler (root :uri "/") (name)
  (concatenate 'string
               "<h1>Hello "
               (or name "Docker")
               "!</h1>"))

(defun start-server ()
  (defparameter server
    (start (make-instance 'easy-acceptor
                          :port 8081)))
  (bt:join-thread (find "hunchentoot-listener"
                        (bt:all-threads)
                        :key 'bt:thread-name
                        :test 'str:starts-with-p)))
