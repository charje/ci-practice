(require 'quicklisp)
(require 'asdf)
;;(ql:update-dist "quicklisp" :prompt nil)
(asdf:load-asd "/usr/src/app/hello.asd")
(ql:quickload "hello")
(sb-ext:save-lisp-and-die
 "hello"
 :toplevel 'hello:start-server
 :executable t)
