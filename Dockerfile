FROM rigetti/quicklisp
WORKDIR /usr/src/app
COPY . .
RUN sbcl --load build.lisp --non-interactive
EXPOSE 8081
CMD ./hello
