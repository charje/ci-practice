(defpackage hello-asd
  (:use #:cl #:asdf))
(in-package #:hello-asd)

(defsystem hello
  :depends-on (#:hunchentoot #:str)
  :components ((:file "package")
               (:file "hello" :depends-on ("package"))))
