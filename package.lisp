(defpackage hello
  (:use #:cl)
  (:import-from #:hunchentoot
                define-easy-handler
                start
                easy-acceptor)
  (:export start-server))
